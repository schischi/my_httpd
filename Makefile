C_FILES= src/http.c \
	src/http_forger.c \
	src/http_parser.c \
	src/main.c \
	src/mime.c \
	src/misc.c \
	src/network.c
CC=clang
FLAG=-g -Wall
LIB=-lpthread -lm
EXEC=httpd

all: $(EXEC)

$(EXEC): $(C_FILES)
	$(CC) -o $@ $^ $(FLAG) $(LDFLAGS) $(LIB)

.SUFFIXES: .c .o
.c.o:
	${COMP} ${FLAG} -c $<

clean:
	rm -vf *.o

nuke: clean
	rm -vf ${EXEC}
