#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "http_parser.h"

static int http_parseRequestElt(s_string* s, char** ptr, char **saveptr,
        char* elt)
{
    char *token;
    token = strtok_r(*ptr, " \r\n", saveptr);
    if(!token) {
        fprintf(stderr, "No %s found\n", elt);
        return -1;
    }
    s->str = token;
    s->len = *saveptr - *ptr - 1;
    int ret = *saveptr - *ptr;
    *ptr = *saveptr;
    return ret;
}

static int http_parseRequest(char* msg, t_http h)
{
    char* token;
    char *saveptr = NULL;
    char* ptr = msg;
    int ret = 0;

    ret += http_parseRequestElt(&h->request->method, &ptr, &saveptr, "method");
    ret += http_parseRequestElt(&h->request->url, &ptr, &saveptr, "url");
    ret += http_parseRequestElt(&h->request->version, &ptr, &saveptr, "version");
    //check cr

    return ret + 1;
}

static int http_parseHeader(char *msg, t_http h)
{
    int i = 0;

    //todo check len of msg (msg->string)
    while(msg[i] != '\r' && msg[i+1] != '\n') {
        t_http_header hdr = malloc(sizeof(struct s_http_header));
        hdr->name.str = msg + i;
        hdr->name.len = i;
        while(msg[i] != '\r' && msg[i+1] != '\n') {
            if(msg[i] == ':') {
                hdr->name.len = i - hdr->name.len;
                hdr->value.str = msg + i + 1;
                hdr->value.len = i + 1;
            }
            i++;
        }
        hdr->value.len = i - hdr->value.len;
        hdr->next = h->header;
        h->header = hdr;
        i += 2;
    }
    return i + 2;
}

s_string *http_getHeaderValue(const char* name, t_http h)
{
    t_http_header hdr;

    hdr = h->header;
    while(hdr) {
        if(!strcmp(hdr->name.str, name))
            return &hdr->value;
        hdr = hdr->next;
    }
    return NULL;
}

static int http_parseBody(char *msg, t_http h)
{
    int len;
    s_string* tmp;
    tmp = http_getHeaderValue("Content-Length", h);
    //todo 0 -> len_packet - current_len
    len = tmp ? strtol(tmp->str, NULL, 10) : 0;
    h->body->data.str = msg;
    h->body->data.len = len;
    return 0;
}

t_http http_parse(char* msg, size_t len)
{
    t_http h;
    int offset;

    h = malloc(sizeof(struct s_http));
    h->request = malloc(sizeof(struct s_http_request));
    h->body = malloc(sizeof(struct s_http_body));
    offset = http_parseRequest(msg, h);
    offset += http_parseHeader(msg + offset, h);
    http_parseBody(msg + offset, h);
    return h;
}

