#include <stdlib.h>
#include <stdio.h>

#include "http_parser.h"
#include "http_forger.h"
#include "network.h"
#include "mime.h"

int main(int argc, const char *argv[])
{
    network_mainLoop();

    return 0;
}
