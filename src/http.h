#ifndef HTTP_H
# define HTTP_H

# include "string.h"

typedef struct s_string s_string;
struct s_string {
    char* str;
    int len;
};

typedef struct s_http_request* t_http_request;
struct s_http_request {
    s_string method;
    s_string url;
    s_string version;
};

typedef struct s_http_answer* t_http_answer;
struct s_http_answer {
    s_string version;
    s_string code;
    s_string value;
};

typedef struct s_http_header* t_http_header;
struct s_http_header {
    s_string name;
    s_string value;
    t_http_header next;
};

typedef struct s_http_body* t_http_body;
struct s_http_body {
    s_string data;
};

typedef struct s_http* t_http;
struct s_http {
    union {
        t_http_request request;
        t_http_answer answer;
    };
    t_http_header header;
    t_http_body body;
};

void http_print(t_http h);
int http_getTotalLen(t_http h);

#endif /* !HTTP_H */
