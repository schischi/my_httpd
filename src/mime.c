#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "mime.h"

int mime_search(char buf[], int len, char* ext)
{
    FILE* f;
    char line[255];
    f = fopen("mime.types", "r");
    while (fgets(line, 255, f) != NULL) {
        char *rest;
        char *token;
        char *mime = strtok_r(line, " \t", &rest);

        while((token = strtok_r(rest, " \t", &rest))) {
            if(!strcmp(token, ext)) {
                strncpy(buf, mime, len);
                return 0;
            }
        }
    }
    return -1;
}
