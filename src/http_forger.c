#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "http_forger.h"
#include "mime.h"

void http_addDefaultHeaders(t_http h)
{
    return;
}

void http_addHeader(t_http h, char* name, char* value)
{
    t_http_header hdr;
    hdr = malloc(sizeof(struct s_http_header));
    hdr->name.str = strdup(name);
    hdr->name.len = strlen(name);
    hdr->value.str = strdup(value);
    hdr->value.len = strlen(value);
    hdr->next = h->header;
    h->header = hdr;
}

static void http_addHeaderDate(t_http h)
{
    time_t date;
    char buf[50];
    date = time(NULL);
    strftime(buf, 50, "%a, %d %b %Y %T", localtime(&date));
    http_addHeader(h, "Date", buf);
}


static void http_addHeaderMime(t_http h, char* file)
{
    char buf[150];
    char *e = strrchr (file, '.');
    if (!e)
        return;
    mime_search(buf, 150, ++e);
    http_addHeader(h, "Content-Type", buf);
}

static void http_addHeaderLength(t_http h, int len)
{
    char buf[10];
    snprintf(buf, 10, "%d", len);
    http_addHeader(h, "Content-Length", buf);
}

void http_addBodyFromFile(t_http h, char* path)
{
    FILE* f;
    char c;
    int i = 0;
    int rea = 1;
    f = fopen(path, "r");
    h->body = malloc(sizeof(struct s_http_body));
    if(!f)
        ;//404
    h->body->data.str = malloc(sizeof(char) * 512);
    while((c = getc(f)) != EOF) {
        h->body->data.str[i] = c;
        ++i;
        if(i > 512 * rea)
            h->body->data.str = realloc(h->body->data.str,
                    sizeof(char) * 512 * ++rea);
    }
    h->body->data.len = i;
    http_addHeaderLength(h, i);
    http_addHeaderMime(h, path);
}

t_http http_forge(char* value, char* code)
{
    t_http h;
    h = malloc(sizeof(struct s_http));
    h->answer = malloc(sizeof(struct s_http_answer));
    h->answer->code.str = code;
    h->answer->code.len = strlen(code);
    h->answer->version.str = "HTTP/1.1";
    h->answer->version.len = 8;
    h->answer->value.str = value;
    h->answer->value.len = strlen(value);
    http_addHeaderDate(h);
    return h;
}

# define ADD_STRING(buf, s) strncpy(buf, s.str, s.len); \
    buf += s.len;
# define ADD_CHAR(c) buf++[0] = c ;
# define ADD_CRLF() buf++[0] = '\r'; \
                               buf++[0] = '\n';

s_string http_toString(t_http h)
{
    s_string str;
    char* buf;
    str.len = http_getTotalLen(h);
    str.str = malloc(sizeof(char) * str.len);
    buf = str.str;

    ADD_STRING(buf, h->answer->version);
    ADD_CHAR(' ');
    ADD_STRING(buf, h->answer->value);
    ADD_CHAR(' ');
    ADD_STRING(buf, h->answer->code);
    ADD_CRLF();
    t_http_header hdr;
    for(hdr = h->header; hdr; hdr = hdr->next) {
        ADD_STRING(buf, hdr->name);
        ADD_CHAR(':');
        ADD_CHAR(' ');
        ADD_STRING(buf, hdr->value);
        ADD_CRLF();
    }
    ADD_CRLF();
    ADD_STRING(buf, h->body->data);
    return str;
}

