#include <stdio.h>

#include "http.h"

void http_print(t_http h)
{
    fprintf(stderr, "REQUEST:\n");
    fprintf(stderr, "\tMethod  : %.*s\n", h->request->method.len,
            h->request->method.str);
    fprintf(stderr, "\tUrl     : %.*s\n", h->request->url.len,
            h->request->url.str);
    fprintf(stderr, "\tversion : %.*s\n", h->request->version.len,
            h->request->version.str);
    fprintf(stderr, "HEADER:\n");
    t_http_header hdr = h->header;
    while(hdr) {
        fprintf(stderr, "\t%.*s : %.*s \n", hdr->name.len, hdr->name.str,
                hdr->value.len, hdr->value.str);
        hdr = hdr->next;
    }
    fprintf(stderr, "BODY:\n");
    fprintf(stderr, "%s", h->body->data.str);
}

int http_getTotalLen(t_http h)
{
    int len;
    t_http_header hdr;
    len = h->answer->code.len + h->answer->value.len + h->answer->version.len;
    len += 2;
    for(hdr = h->header; hdr; hdr = hdr->next)
        len += hdr->name.len + hdr->value.len + 5;
    len += 2;
    len += h->body->data.len;
    return len;
}
