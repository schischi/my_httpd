#define _SVID_SOURCE
#include <fcntl.h>
#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/select.h>

#include "network.h"
#include "http_parser.h"
#include "http_forger.h"

static volatile int exit_request;

static void signal_handler(int sig)
{
    fprintf(stderr, "SIGTERM\n");
    exit_request = 1;
}

int network_init(int *sd, sigset_t *orig_mask)
{
    struct sockaddr_in server;

    *sd = socket(AF_INET, SOCK_STREAM, 0);
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(8080);
    bind(*sd, (struct sockaddr*)&server, sizeof(server));

    if (listen(*sd, 5) == -1) {
        perror("listen");
        return -1;
    }

    sigset_t mask;
    struct sigaction act;
    sigemptyset(&mask);
    sigaddset(&mask, SIGTERM);

    memset(&act, 0, sizeof(act));
    act.sa_handler = signal_handler;
    if(sigaction(SIGTERM, &act, 0)) {
        perror("sigaction");
        return -1;
    }
    if(sigprocmask(SIG_BLOCK, &mask, orig_mask) < 0) {
        perror("sigprocmask");
        return -1;
    }
    return 0;
}

void handleConnection(int sd, int *highest, int clients[])
{
    int fd, i;
    struct sockaddr_in addr;
    socklen_t sin_size;
    if ((fd = accept(sd, (struct sockaddr *)&addr, &sin_size)) == -1) {
        perror("accept");
        return;
    }
    for( i = 0 ; i < 10 ; i ++ )
        if (clients[i] == 0) {
            clients[i] = fd ;
            break;
        }
    if (i != 10) {
        if (fd > *highest)
            *highest = clients[i];
        fprintf(stderr, "New connexion\n");
    }    
    else {
        fprintf(stderr, "Too much clients\n");
        close(fd);  
    }
}

void handleQuery(int *clientfd)
{
    int n;
    char buf[65536];
    if((n = recv(*clientfd,buf, 65536,0)) <= 0 ) {
        printf("Connexion lost from slot"); 
        close(*clientfd);
        *clientfd = 0 ;
    }
    else {
        t_http r, s;
        r = http_parse(buf, n);

        s = http_forge("200", "OK");
        http_addBodyFromFile(s, "test.html");
        s_string str = http_toString(s);
        send(*clientfd, str.str, str.len + 2, 0);
    }
}

void network_mainLoop()
{
    fd_set rfds;
    int sd, highest, i;
    int clients[10];
    sigset_t orig_mask;

    network_init(&sd, &orig_mask);
    highest = sd;
    memset(clients, 0, sizeof(clients));

    while(!exit_request) {
        FD_ZERO(&rfds);
        for(i = 0; i < 10; ++i)
            if(clients[i] != 0)
                FD_SET(clients[i], &rfds);
        FD_SET(sd, &rfds);
        if(pselect(highest + 1, &rfds, NULL, NULL, NULL, &orig_mask) < 0)
            continue;
        //error
        if(FD_ISSET(sd, &rfds))
            handleConnection(sd, &highest, clients);
        for(i = 0; i < 10; ++i)
            if(FD_ISSET(clients[i], &rfds))
                handleQuery(&clients[i]);
    }
}
