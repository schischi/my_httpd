#ifndef HTTP_FORGER_H
# define HTTP_FORGER_H

# include "http.h"

s_string http_toString(t_http h);
t_http http_forge(char* value, char* code);
void http_addBodyFromFile(t_http h, char* path);

#endif /* !HTTP_FORGER_H */
