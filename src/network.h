#ifndef NETWORK_H
# define NETWORK_H

# include <signal.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <arpa/inet.h>

void network_mainLoop();

#endif /* !NETWORK_H */
