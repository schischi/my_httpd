#ifndef HTTP_PARSER_H
# define HTTP_PARSER_H

# include "http.h"

t_http http_parse(char* msg, size_t len);
s_string* http_getHeaderValue(const char* name, t_http h);

#endif /* !HTTP_PARSER_H */
